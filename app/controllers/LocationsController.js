const LocationsController = module.exports;
const LocationsServices = require('../services/LocationsServices');
const log4j = require('../utils/logger');
const LogUtils = require('../utils/LogUtils');
const Validator = require('../utils/Validator');

// Schemas
const DistanceCreateSchema = require('../model/requests/DistancesCreateSchema');
const CoverageUserAddressSchema = require('../model/requests/CoverageUserAddressSchema');

/**
 * @swagger
 *
 * /distance:
 *   post:
 *     tags:
 *      - distance calculate
 *     description: calculated distance between two points
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: "body"
 *         in: "body"
 *         description: "body of request of distnace"
 *         required: true
 *         schema:
 *           name: origins and destinations
 *           type: array
 *           require: true
 *     responses:
 *       '200':
 *         description: OK
 *       '400':
 *         description: Bad request. Error in params
 *       '401':
 *         description: Authorization information is missing or invalid.
 *       '404':
 *         description: Entity not found.
 *       '5XX':
 *         description: Unexpected error.
 */
LocationsController.calculateDistance = (req, res, next) => {
  const logName = 'calculate distances matriz: ';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info(`Starts UsersController.create: params ${JSON.stringify(body)}`);
  Validator(DistanceCreateSchema).validateRequest(body);

  return LocationsServices.calculateDistance(body)
    .then(response => res.send(response))
    .catch(error => next(error));
};

/**
 * @swagger
 *
 * /stores/coverage:
 *   post:
 *     tags:
 *      - choose nearby store
 *     description: choose nearby store
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: "body"
 *         in: "body"
 *         description: "body of request with lat and lng user"
 *         required: true
 *         schema:
 *           name1: lat y lng
 *           type: decimal
 *           require: true
 *     responses:
 *       '200':
 *         description: OK
 *       '400':
 *         description: Bad request. Error in params
 *       '401':
 *         description: Authorization information is missing or invalid.
 *       '404':
 *         description: Entity not found.
 *       '5XX':
 *         description: Unexpected error.
 */
LocationsController.chooseNearbyStore = (req, res, next) => {
  const logName = 'choose nearby store: ';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info(`Starts DistnacesController.create: params ${JSON.stringify(body)}`);
  Validator(CoverageUserAddressSchema).validateRequest(body);

  return LocationsServices.chooseNearbyStore(body)
    .then(response => res.send(response))
    .catch(error => next(error));
};

/**
 * @swagger
 *
 * /addresses:
 *   get:
 *     tags:
 *      - Addresses
 *     description: show possible addresses
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: query
 *         schema:
 *           type: string
 *         description: search base text to auto-complete
 *         require: false
 *     responses:
 *       '200':
 *         description: OK
 *       '400':
 *         description: Bad request. Error in params
 *       '401':
 *         description: Authorization information is missing or invalid.
 *       '404':
 *         description: Entity not found.
 *       '5XX':
 *         description: Unexpected error.
 */
LocationsController.findAddresses = (req, res, next) => {
  const logName = 'show possible addresses: ';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { query: { query } } = req;
  logger.info(`Starts AddressesController.findAddresses: query ${JSON.stringify(query)}`);

  return LocationsServices.findAddresses(query, { logger, logName })
    .then(response => res.send(response))
    .catch(error => next(error));
};

/**
 * @swagger
 *
 * /geolocation:
 *   get:
 *     tags:
 *      - Geolocation
 *     description:  location of an address
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: query
 *         schema:
 *           type: string
 *         description: Address indicated to give the location
 *         required: true
 *     responses:
 *       '200':
 *         description: OK
 *       '400':
 *         description: Bad request. Error in params
 *       '401':
 *         description: Authorization information is missing or invalid.
 *       '404':
 *         description: Entity not found.
 *       '5XX':
 *         description: Unexpected error.
 */
LocationsController.geographicalLocationAddress = (req, res, next) => {
  const logName = 'geographical location of an address: ';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { query: { query } } = req;
  logger.info(`Starts LocationsController.geographicalLocationAddress: query ${JSON.stringify(query)}`);

  return LocationsServices.geographicalLocationAddress(query, { logger, logName })
    .then(response => res.send(response))
    .catch(error => next(error));
};
