const LocationsService = module.exports;
const Promise = require('bluebird');
const StoresMSResource = require('../resources/StoresMSResource');
const CalculateMatrixDistanceMSResources = require('../resources/CalculatedMatrixDIstanceMSResources');
const log4j = require('../utils/logger');
const AddressesMSResource = require('../resources/AddressesMSResource');

const defaultLogger = log4j.getLogger('OrdersService');

LocationsService.calculateDistance = body => Promise.mapSeries(body.paths, path =>
  CalculateMatrixDistanceMSResources.calculateMatrixDistance(
    `${path.point1.lat},${path.point1.lng}`,
    `${path.point2.lat},${path.point2.lng}`,
  ));

LocationsService.chooseNearbyStore = async (body, options = {}) => {
  const { logger = defaultLogger } = options;
  const storeInfo = await StoresMSResource.getAllStores();
  logger.info(`yyyy ${JSON.stringify(storeInfo)}`);
  const arr = [];
  const result = await Promise.mapSeries(storeInfo, async (store) => {
    const { latitude, longitude } = store;
    const dictance = await CalculateMatrixDistanceMSResources.calculateMatrixDistance(
      `${body.lat},${body.lng}`,
      `${latitude},${longitude}`,
    );

    logger.info(`xxx ${JSON.stringify(dictance)}`);
    if (dictance < store.coverage) {
      return store;
    }

    return null;
  });
  result.forEach((res) => {
    if (res !== null) {
      arr.push(res);
    }
  });
  logger.info(`tamaño ${JSON.stringify(arr.length)}`);

  return arr;
};

LocationsService.findAddresses = async (query) => {
  const { predictions } = await AddressesMSResource.findAddresses(query);

  return predictions.map(element => ({
    description: element.description,
    main_text: element.structured_formatting.main_text,
    terms: element.terms,
  }));
};

LocationsService.geographicalLocationAddress = async (query) => {
  const { results } = await AddressesMSResource.geographicalLocationAddress(query);

  return results.map(element => ({
    location: element.geometry.location,
  }));
};
