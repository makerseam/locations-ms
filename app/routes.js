const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const SwaggerConfig = require('./config/SwaggerConfig');
const LocationsController = require('./controllers/LocationsController');

const swaggerSpec = swaggerJsdoc(SwaggerConfig.option);
const router = express.Router();

router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerSpec));

router.post('/distance', LocationsController.calculateDistance);
router.post('/stores/coverage', LocationsController.chooseNearbyStore);
router.get('/addresses', LocationsController.findAddresses);
router.get('/geolocation', LocationsController.geographicalLocationAddress);

module.exports = router;

