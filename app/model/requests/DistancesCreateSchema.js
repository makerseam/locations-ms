module.exports = {
  required: [
    'paths',
  ],
  properties: {
    paths: {
      type: 'array',
      title: 'The Paths Schema',
      default: [],
      items: {
        type: 'object',
        default: {},
        examples: [
          {
            point2: {
              lng: -75.573553,
              lat: 6.2443382,
            },
            point1: {
              lng: -75.6723751,
              lat: 4.536307,
            },
          },
        ],
        required: [
          'point1',
          'point2',
        ],
        properties: {
          point1: {
            type: 'object',
            default: {},
            examples: [
              {
                lat: 4.536307,
                lng: -75.6723751,
              },
            ],
            required: [
              'lat',
              'lng',
            ],
            properties: {
              lat: {
                type: 'number',
                default: 0,
                examples: [
                  4.536307,
                ],
              },
              lng: {
                type: 'number',
                default: 0,
                examples: [
                  -75.6723751,
                ],
              },
            },
          },
          point2: {
            type: 'object',
            default: {},
            examples: [
              {
                lng: -75.573553,
                lat: 6.2443382,
              },
            ],
            required: [
              'lat',
              'lng',
            ],
            properties: {
              lat: {
                type: 'number',
                default: 0,
                examples: [
                  6.2443382,
                ],
              },
              lng: {
                type: 'number',
                default: 0,
                examples: [
                  -75.573553,
                ],
              },
            },
          },
        },
      },
    },
  },
};
