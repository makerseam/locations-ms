module.exports = {
  type: 'object',
  required: [
    'lat',
    'lng',
  ],
  properties: {
    lat: {
      type: 'number',
      examples: [
        4.536307,
      ],
    },
    lng: {
      type: 'number',
      examples: [
        -75.6723751,
      ],
    },
  },
};
