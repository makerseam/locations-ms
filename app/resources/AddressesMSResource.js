const AddressesMSResource = module.exports;
const HTTPClient = require('../utils/HTTPClient');

const {
  GOOGLE_API_HOST,
  GOOGLE_API_KEY,
} = process.env;
const BASE_URL = `${GOOGLE_API_HOST}`;
const key = `${GOOGLE_API_KEY}`;

AddressesMSResource.findAddresses = query =>
  HTTPClient.get(`${BASE_URL}place/autocomplete/json?input=${query}
  &types=geocode&language=es&key=${key}&locationbias=lat/lng`);

AddressesMSResource.geographicalLocationAddress = query =>
  HTTPClient.get(`${BASE_URL}geocode/json?address=${query}
  &types=geocode&language=es&key=${key}&locationbias=lat/lng`);
