const StoresMSResource = module.exports;
const HTTPClient = require('../utils/HTTPClient');

const { MICROSERVICE_URL } = process.env;
const BaseURL = `${MICROSERVICE_URL}/api/stores-ms`;

StoresMSResource.getAllStores = () =>
  HTTPClient.get(`${BaseURL}/stores`);
