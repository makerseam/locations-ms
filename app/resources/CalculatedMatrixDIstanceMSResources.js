const CalculateMatrixDistanceMSResources = module.exports;

const googleDistance = require('google-distance-matrix');

CalculateMatrixDistanceMSResources.calculateMatrixDistance = (origins, destinations) =>
  new Promise((resolve, reject) => {
    googleDistance.key(process.env.GOOGLE_API_KEY);
    googleDistance.matrix([origins], [destinations], (err, distances) => {
      if (err) return reject(err);
      const result = distances.rows[0].elements[0].distance.text;

      return resolve(parseInt(result, 10));
    });
  });
