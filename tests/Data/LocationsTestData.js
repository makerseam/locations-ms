const LocationsTestData = module.exports;

LocationsTestData.getResposeLocations = () => ([
  {
    description: 'Armenia, Quindío, Colombia',
    main_text: 'Armenia',
    terms: [
      {
        offset: 0,
        value: 'Armenia',
      },
      {
        offset: 9,
        value: 'Quindío',
      },
      {
        offset: 18,
        value: 'Colombia',
      },
    ],
  },
]);

LocationsTestData.getResponseGeolocation = () => ([
  {
    location: {
      lat: 4.535000399999999,
      lng: -75.6756888,
    },
  },
]);
