const StoresTestData = module.exports;

StoresTestData.getStores = () => ({
  stores: [
    {
      store_id: 1,
      address: 'cra 13',
      longitude: -75.662523,
      latitude: 4.540729,
      city_address_id: '1',
      brand: 'nose',
      store_type_id: 1,
      coverage: 50,
    },
    {
      store_id: 2,
      address: 'cra 13',
      longitude: -75.708188,
      latitude: 4.514035,
      city_address_id: 1,
      brand: 'nose',
      store_type_id: 1,
      coverage: 150,
    },
  ],
});

StoresTestData.getAllStores = () => ([
  {
    store_id: 1,
    address: 'cra 13',
    longitude: -75.662523,
    latitude: 4.540729,
    city_address_id: '1',
    brand: 'nose',
    store_type_id: 1,
    coverage: 50,
  },
  {
    store_id: 2,
    address: 'cra 13',
    longitude: -75.708188,
    latitude: 4.514035,
    city_address_id: 1,
    brand: 'nose',
    store_type_id: 1,
    coverage: 150,
  },
  {
    store_id: 3,
    address: 'cra 13',
    longitude: -75.663359,
    latitude: 4.544180,
    city_address_id: '1',
    brand: 'nose',
    store_type_id: 1,
    coverage: 2,
  },
]);

StoresTestData.getPoints = () => ({
  paths: [
    {
      point1: {
        lat: 4.514035,
        lng: -75.708188,
      },
      point2: {
        lat: 4.546377,
        lng: -75.686558,
      },
    },
    {
      point1: {
        lat: 4.514035,
        lng: -75.708188,
      },
      point2: {
        lat: 4.579576,
        lng: -75.644836,
      },
    },
  ],
});

StoresTestData.getAnswers = () => ({
  result: [
    16,
    1,
    16,
    24,
  ],
});

StoresTestData.getCoorUser = () => ({
  lat: 4.536307,
  lng: -75.6723751,
});

StoresTestData.getRespose = () => ([
  {
    store_id: 1,
    address: 'cra 13',
    longitude: -75.662523,
    latitude: 4.540729,
    city_address_id: 1,
    brand: 'nose',
    store_type_id: 1,
    coverage: 10,
  },
  {
    store_id: 2,
    address: 'cra 13',
    longitude: -75.708188,
    latitude: 4.514035,
    city_address_id: 1,
    brand: 'nose',
    store_type_id: 1,
    coverage: 15,
  },
  {
    store_id: 3,
    address: 'cra 13',
    longitude: -75.686558,
    latitude: 4.546377,
    city_address_id: 1,
    brand: 'nose',
    store_type_id: 1,
    coverage: 7,
  },
]);
