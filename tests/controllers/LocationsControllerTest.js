const assert = require('assert');
const chai = require('chai');
const sandbox = require('sinon').createSandbox();
const StoresMSResources = require('../../app/resources/StoresMSResource');
const chaiHttp = require('chai-http');
const app = require('../../index');
const StoresTestData = require('../Data/StoresTestData');
const LocationsTestData = require('../Data/LocationsTestData');
const CalculateMatrixDistanceMSResources = require('../../app/resources/CalculatedMatrixDIstanceMSResources');
const log4j = require('../../app/utils/logger');
const AddressesMSResource = require('../../app/resources/AddressesMSResource');

const defaultLogger = log4j.getLogger('LocationsTestController');

console.log({ app, assert });
chai.use(chaiHttp);
const API = '/api/locations-ms/';

describe('Locations test', () => {
  afterEach(() => {
    sandbox.restore();
  });

  it('test if the points do not send', () => chai
    .request(app)
    .post(`${API}/distance`)
    .send({})
    .then(assert.fail)
    .catch((error) => {
      assert.notEqual(error.status, 404);
    }));

  it('calculate distances test', async () => {
    const body = StoresTestData.getPoints();
    const {
      paths: [{
        point1:
        {
          lat,
          lng,
        },
        point2:
        {
          lat: lat2,
          lng: lng2,
        },
      },
      {
        point1:
          {
            lat: lat3,
            lng: lng3,
          },
        point2:
          {
            lat: lat4,
            lng: lng4,
          },
      },
      ],
    } = body;
    const mock = sandbox.stub(CalculateMatrixDistanceMSResources, 'calculateMatrixDistance');
    mock.withArgs(`${lat},${lng}`, `${lat2},${lng2}`).returns(16);
    mock.withArgs(`${lat3},${lng3}`, `${lat4},${lng4}`).returns(24);

    return chai
      .request(app)
      .post(`${API}distance`)
      .send(body)
      .then((response) => {
        defaultLogger.info(`xxxx ${JSON.stringify(response)}`);
      });
  });

  it('with out coordinates of user test', () => chai
    .request(app)
    .post(`${API}stores/coverage`)
    .send({})
    .then(assert.fail)
    .catch((error) => {
      assert.notEqual(error.status, 404);
    }));

  it('Choose Nearvy store test', async () => {
    const body = StoresTestData.getCoorUser();
    const { lat: latU, lng: lngU } = body;
    const data = StoresTestData.getAllStores();
    const [{ latitude, longitude }, { latitude: lat2, longitude: lng2 }] = data;
    sandbox.stub(StoresMSResources, 'getAllStores').returns(StoresTestData.getAllStores());
    const mock = sandbox.stub(CalculateMatrixDistanceMSResources, 'calculateMatrixDistance');
    mock.withArgs(`${latU},${lngU}`, `${latitude},${longitude}`).returns(16);
    mock.withArgs(`${latU},${lngU}`, `${lat2},${lng2}`).returns(24);

    return chai
      .request(app)
      .post(`${API}stores/coverage`)
      .send(body)
      .then((response) => {
        defaultLogger.info(`oooooooooooooooo ${JSON.stringify(response.body)}`);
        assert.deepEqual(
          response.body,
          [
            {
              store_id: 1,
              address: 'cra 13',
              longitude: -75.662523,
              latitude: 4.540729,
              city_address_id: 1,
              brand: 'nose',
              store_type_id: 1,
              coverage: 50,
            },
            {
              store_id: 2,
              address: 'cra 13',
              longitude: -75.708188,
              latitude: 4.514035,
              city_address_id: 1,
              brand: 'nose',
              store_type_id: 1,
              coverage: 150,
            }],
        );
      });
  });

  it('autocomplete address', async () => {
    const query = 'armenia';
    defaultLogger.info(`query ${JSON.stringify(query)}`);
    sandbox.stub(AddressesMSResource, 'findAddresses').resolves(LocationsTestData.getResposeLocations());

    chai.request(app)
      .get(`${API}addresses?query=${query}`)
      .then((response) => {
        assert.equal(
          response.body,
          [
            {
              description: 'Armenia, Quindío, Colombia',
              main_text: 'Armenia',
              terms: [
                {
                  offset: 0,
                  value: 'Armenia',
                },
                {
                  offset: 9,
                  value: 'Quindío',
                },
                {
                  offset: 18,
                  value: 'Colombia',
                },
              ],
            }],
        );
      });
  });

  it('test without address', async () => {
    const query = '';
    defaultLogger.info(`query ${JSON.stringify(query)}`);
    sandbox.stub(AddressesMSResource, 'findAddresses').resolves([]);
    chai.request(app)
      .get(`${API}addresses?query=${query}`)
      .then((response) => {
        assert.equal(
          response.body,
          [],
        );
      });
  });

  it('incomplete uri test', async () => chai
    .request(app)
    .get(`${API}addr?query=`)
    .then(assert.fail)
    .catch(error => assert.equal(error.status, 404)));

  it('misspelled uri test', async () => chai
    .request(app)
    .get(`${API}addressescalle`)
    .then(assert.fail)
    .catch(error => assert.equal(error.status, 404)));

  it('bad submitted query test', async () => chai
    .request(app)
    .get(`${API}addressesquery=calle`)
    .then(assert.fail)
    .catch(error => assert.equal(error.status, 404)));
});

it('get geolocation an address', async () => {
  const query = 'Armenia, Quindío, Colombia';
  defaultLogger.info(`query ${JSON.stringify(query)}`);
  sandbox.stub(AddressesMSResource, 'geographicalLocationAddress')
    .resolves(LocationsTestData.getResponseGeolocation());

  chai.request(app)
    .get(`${API}geolocation?query=${query}`)
    .then((response) => {
      defaultLogger.info(`${JSON.stringify(response.info)}`);
      assert.equal(
        response.body,
        [
          {
            location: {
              lat: 4.535000399999999,
              lng: -75.6756888,
            },
          }],
      );
    });
});

it('test without address geolocation', async () => chai
  .request(app)
  .get(`${API}geolocation?query=`)
  .then(assert.fail)
  .catch(error => assert.equal(error.status, 500)));

it('bad endpoint test', async () => chai
  .request(app)
  .get(`${API}geolocaquery=Bogotá, Colombia`)
  .then(assert.fail)
  .catch(error => assert.equal(error.status, 404)));
